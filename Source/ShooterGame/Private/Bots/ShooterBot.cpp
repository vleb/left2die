// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Bots/ShooterBot.h"
#include "Bots/ShooterAIController.h"
#include "Kismet/GameplayStatics.h"

AShooterBot::AShooterBot(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	AIControllerClass = AShooterAIController::StaticClass();

	UpdatePawnMeshes();

	bUseControllerRotationYaw = true;

	static ConstructorHelpers::FObjectFinder<USoundBase> Zombie0Sound(TEXT("/Game/Menu/ReworkedAssets/zombie0"));
	static ConstructorHelpers::FObjectFinder<USoundBase> Zombie1Sound(TEXT("/Game/Menu/ReworkedAssets/zombie1"));
	static ConstructorHelpers::FObjectFinder<USoundBase> ZombieDeathSound(TEXT("/Game/Menu/ReworkedAssets/zombie-death"));
	static ConstructorHelpers::FObjectFinder<USoundAttenuation> SoundAttenuationData(TEXT("/Game/Blueprints/Pawns/BotSoundAttenuation"));

	Zombie0 = Zombie0Sound.Object;
	Zombie1 = Zombie1Sound.Object;
	ZombieDeath = ZombieDeathSound.Object;
	SoundAttenuation = SoundAttenuationData.Object;

	PSC = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particles"));
	PSC->AttachTo(GetMesh());
	PSC->bAutoActivate = false;
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAsset(TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion"));
	if(ParticleAsset.Succeeded()) {
		PSC->SetTemplate(ParticleAsset.Object);
	}
}

bool AShooterBot::IsFirstPerson() const
{
	return false;
}

void AShooterBot::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);

	Super::FaceRotation(CurrentRotation, DeltaTime);
}

void AShooterBot::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimerHandle, this, &AShooterBot::PlayBotSound, 2.0f, true, 0.0f);
}

void AShooterBot::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorldTimerManager().ClearTimer(TimerHandle);
}

void AShooterBot::PlayBotSound()
{
	if (IsAlive())
		UGameplayStatics::PlaySoundAtLocation(this, FMath::RandRange(0, 1) ? Zombie0 : Zombie1, this->GetActorLocation(), 1.0f, FMath::RandRange(0.5f, 2.0f) * GetActorScale().Y, 0.0f, SoundAttenuation);
}

void AShooterBot::OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser)
{
	Super::OnDeath(KillingDamage, DamageEvent, InstigatingPawn, DamageCauser);

	float scale = GetActorScale().X;
	if (scale > 1.0f) {
		PSC->ActivateSystem(true);
	}

	UGameplayStatics::PlaySoundAtLocation(this, ZombieDeath, this->GetActorLocation(), 1.0f, FMath::RandRange(0.5f, 2.0f) * GetActorScale().Y, 0.0f, SoundAttenuation);
}