// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "ShooterBot.generated.h"

UCLASS()
class AShooterBot : public AShooterCharacter
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category=Behavior)
	class UBehaviorTree* BotBehavior;

	virtual bool IsFirstPerson() const override;

	virtual void FaceRotation(FRotator NewRotation, float DeltaTime = 0.f) override;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type) override;
	virtual void OnDeath(float, struct FDamageEvent const&, class APawn*, class AActor*) override;

	void PlayBotSound();

	UPROPERTY()
	FTimerHandle TimerHandle;

	UPROPERTY()
	USoundBase* Zombie0;

	UPROPERTY()
	USoundBase* Zombie1;

	UPROPERTY()
	USoundBase* ZombieDeath;

	UPROPERTY()
	USoundAttenuation* SoundAttenuation;
	
	UPROPERTY(VisibleAnywhere)
	UParticleSystemComponent* PSC;
};